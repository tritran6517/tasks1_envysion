/*global require*/
"use strict";

var twig = require('gulp-twig'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    del = require('del'),
    sass = require('gulp-sass'),
    wait = require('gulp-wait'),
    gulp = require('gulp');

    sass.compiler = require('node-sass');

var paths = {
    build: './dist/',
};

var pathInput = {
    templateBase:'src/html/',
    template: ['src/html/**/*.twig', 'src/html/**/*.html'],
    css: 'src/css/style.scss',
    fonts: 'src/fonts/**/*',
    images: 'src/images/**/*',
    js: 'src/js/**/*',
    libs: 'src/libs/**/*',
}

var pathOutput ={
    template: './dist/',
    css: './dist/css/',
    fonts: './dist/fonts/',
    images: './dist/images/',
    js: './dist/js/',
    libs: './dist/libs/',
};  

var pathClean = {
    dist:"dist/**/*"
}

gulp.task('twig', function () {
    return gulp.src(pathInput.template, {
            base: pathInput.templateBase
        })
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(twig())
        .on('error', function (err) {
            process.stderr.write(err.message + '\n');
            this.emit('end');
        })
        .pipe(gulp.dest(pathOutput.template));
});

gulp.task('copy-css', function () {
    return gulp.src(pathInput.css)
        .pipe(wait(500))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(pathOutput.css));
});

gulp.task('copy-js', function () {
    return gulp.src(pathInput.js)
        .pipe(gulp.dest(pathOutput.js));
});

gulp.task('copy-fonts', function () {
    return gulp.src(pathInput.fonts)
        .pipe(gulp.dest(pathOutput.fonts));
});

gulp.task('copy-images', function () {
    return gulp.src(pathInput.images)
        .pipe(gulp.dest(pathOutput.images));
});

gulp.task('copy-libs', function () {
    return gulp.src(pathInput.libs)
        .pipe(gulp.dest(pathOutput.libs));
});

gulp.task('clean', function(){
    return del(pathClean.dist, {force:true});
});

gulp.task('rebuild', ['twig', 'copy-css', 'copy-fonts', 'copy-images', 'copy-js', 'copy-libs'], function () {
    browserSync.reload();
});

gulp.task('build', ['clean'],function(){
    gulp.start([ 'twig', 'copy-css', 'copy-fonts', 'copy-images', 'copy-js', 'copy-libs']);
});

gulp.task('browser-sync', ['rebuild'], function () {
    browserSync({
        server: {
            baseDir: paths.build,
            port: 3000
        },
    });
});

gulp.task('watch', function () {
    gulp.watch([
        pathInput.template,
        'src/css/**/*',
        pathInput.fonts,
        pathInput.images,
        pathInput.js,
        pathInput.libs,
    ], {
        cwd: './'
    }, ['rebuild']);
});

gulp.task('default',['browser-sync', 'watch']);