$(document).ready(function () {
    $('.index .you__slide').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [{
            breakpoint: 920,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }, ]
    })

    $('.index .philosophy__slide').slick({
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        focusOnSelect: true,
        asNavFor: '.philosophy__quote',
        responsive: [{
                breakpoint: 920,
                settings: {
                    centerPadding: '80px',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerPadding: '60px',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    })

    $('.index .philosophy__quote').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.philosophy__slide'
    })

    $('.establishment .profile__slide1').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
        asNavFor: '.profile__slide3',
        focusOnSelect: true,
        responsive: [{
            breakpoint: 920,
            settings: {
                slidesToShow: 1
            }
        }]
    })

    $('.establishment .profile__slide2').slick({
        arrows: false,
        dots: false,
        autoplay: true
    })

    $('.establishment .profile__slide3').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.profile__slide1'
    })

    $('.expertise .indepen__slide').slick({
        arrows: true,
        dots: false,
    })


    $('.envysion__slide').slick({
        arrows: false,
        dots: false,
        // autoplay: true,
        // autoplaySpeed: 5000,
        // pauseOnFocus: false,
        // pauseOnHover: false
    })

    $(".nav-header__btn").on('click', function () {
        $(".nav-header__list").slideToggle();
    })

    $(".indepen .expertise__btn").on('click', function () {
        $(this).next().slideToggle();
    })

    $(".philosophy__logo-arrow").on('click', function () {
        $(".philosophy__desc").slideDown();
        $(".philosophy__desc-arrow").slideDown();
        $(this).hide();
    })

    $(".philosophy__desc-arrow").on('click', function () {
        $(".philosophy__desc").slideUp();
        $(this).slideUp();
        setTimeout(() => {
            $(".philosophy__logo-arrow").show();
        }, 450);
    })

    $(window).resize(function () {
        windowWidth = $(this).width();
        if (windowWidth > 920) {
            $(".nav-header__list").css({
                display: 'block'
            })
        } else {
            $(".nav-header__list").css({
                display: 'none'
            })
        }
    });

    $('.mainVisual__logo').hover(function () {
        $('.mainVisual__circle').addClass('is-active');
        $(this).addClass('is-opt');
    })
    $('.mainVisual__circle').mouseleave(function () {
        $('.mainVisual__circle').removeClass('is-active');
        $('.mainVisual__logo').removeClass('is-opt');
    })

    var scrollBanner = function () {
        var banner = $('.mainVisual--banner1');
        var heightCurrent = banner.height();
        var heightSetting = '144vh';
        var pos = $('.mainVisual__btn').offset().top;
        $(window).scroll(function () {
            if ($(window).scrollTop() > pos) {
                $('.mainVisual').addClass('is-scroll');
                $('.mainVisual__circle').removeClass('is-active');
                $('.mainVisual__logo').removeClass('is-opt');
                $('.title2').addClass('is-fade');
            } else {
                $('.mainVisual').removeClass('is-scroll');
            }
        })
    }

    // var isTyping;

    // function typing(element) {
    //     clearTimeout(isTyping);
    //     var defaultText = element.attr('data-text') || "";
    //     console.log(defaultText);
    //     var currentText = '';
    //     for (var i = 0; i < defaultText.length; i++) {

    //         if (defaultText[i].charCodeAt(0) === 10) {
    //             currentText += "<br/>";
    //         } else {
    //             currentText += defaultText[i];
    //         }
    //         updateText(element, currentText, i);

    //         isTyping = setTimeout(function () {
    //             element.text(defaultText);
    //         }, defaultText.length * 80);

    //     }
    // }

    // function updateText(element, text, i) {
    //     setTimeout(function () {
    //         element.html(text);
    //     }, i * 80);
    // }

    $(".typing, .mainVisual__txt").each(function () {
        // typing($(this));
        if($(this).hasClass("disabled") === true){ return false }
        let text = $(this).attr("data-text");
        if(text){
            text = ( text.replace(/<br\s*[\/]?>/gi,'\n') ) ? text.replace(/<br\s*[\/]?>/gi,'\n') : text;
            text = ( text.replace(/\r?\n\n|\r/g,'\n') ) ? text.replace(/\r?\n\n|\r/g,'\n') : text;
            text = ( text.replace(/<p>/g,'') ) ? text.replace(/<p>/g,'') : text;
            text = ( text.replace(/<[^>]*>?/gm, '') ) ? text.replace(/<[^>]*>?/gm, '\n') : text;
        }
        $(this).text(text);
    })

    // $('.envysion__slide').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    //     $(slick.$slides[nextSlide]).find('.mainVisual__txt').addClass('typing');
    //     if ($(slick.$slides[nextSlide]).find('.mainVisual__txt').hasClass('typing')) {
    //         typing($($(slick.$slides[nextSlide]).find('.mainVisual__txt')));
    //         $(slick.$slides[currentSlide]).find('.mainVisual__txt').text(' ');
    //     }
    // });
    // $('.envysion__slide').on('afterChange', function (event, slick, currentSlide) {
    //     $(slick.$slides[currentSlide]).find('.mainVisual__txt').text(' ');
    // });

    var scrollToAddClass = function (ele) {
        $(window).on('scroll', function () {

            var top_of_element = ele.offset().top;
            var bottom_of_element = ele.offset().top + ele.outerHeight();

            var top_of_screen = $(window).scrollTop();
            var view = top_of_screen + ($(window).innerHeight() / 2);

            if ((top_of_element < view) && (view < bottom_of_element)) {
                ele.addClass('is-scroll')
            }
        });
        $(window).trigger("scroll");
    }

    var initScrollToAddClass = function () {
        $(".imgText1__img-quarter, .imgText2__img-quarter").each(function (i, element) {
            scrollToAddClass($(element));
        });
    }

    var initEqualHeight = function(element){
        var equalHeight = function(element){
            element.removeAttr("style");
            var max = 0;
            element.each(function(){
                if($(this).height() > max){
                    max = $(this).height();
                }
            });
            element.height(max);
        }
        equalHeight(element);
        $(window).on('resize' , function(){
            if($(window).outerWidth() <= 920){
                element.removeAttr('style');
                return false;
            }
            equalHeight(element);
        });
    }

    var parallaxBanner = function(){
        function parallax(selector){
            var scrolled = $(window).scrollTop();
            $(selector).css('transform',"translate(0,"+  (scrolled * 1) + 'px' + ")");
        }
    
        $(window).scroll(function(e){
            parallax('.mainVisual__banner:not(".mainVisual__disabled-parallax")');
        });
    }

    var expertiseMeaningSlider = function(){
        if($(window).outerWidth() > 920){ 
            if( $(".meaning__list.slick-initialized.slick-slider").length >= 1 ){
                $('.expertise .meaning__list').slick('unslick');
            }
            return false;
        }
        $('.expertise .meaning__list').not('.slick-initialized').slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            arrows: false,
            dots: false,
        });
    }

    var expertiseMeaningTextSlider = function(){
        $('.meaning__text-slide').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
        })
    }

    
var clickScrollToElement = function(){
    var isScrolling = false;
    if(window.location.hash){
        $([document.documentElement, document.body]).animate({
            scrollTop: $(window.location.hash).offset().top
        }, 700, function(){
            isScrolling = false;
        });
    }
    $(".nav-header__submenu-link").on("click" , function(e){
        if(isScrolling === true){ return false }
        isScrolling = true;
        var id = $(this).attr("href");

        if(id.substring(0, 1) === "#"){
            e.preventDefault();
        }

        $([document.documentElement, document.body]).animate({
            scrollTop: $(id).offset().top
        }, 700, function(){
            isScrolling = false;
        });
    });
}

var animation = function(){
    var arrElement = [
        $(".philosophy h3.title1"),
        $(".principles h3.title1"),
        $(".c-generation__wrap .c-generation__title1"),
        $(".about .title2 "),
        $(".career .title2 "),
        $("#our_leadership > .title2 "),
        $("#executives_profile .title1 "),
    ]
    var listElement= [];
    arrElement.forEach(function(ele){
        if(ele.length == 0){return false}
        if(ele.length > 1){
            listElement.push({
                ele: ele.eq(0),
                hasNext :true
            });
        }else{
            listElement.push(ele);
        }
    });
    listElement.forEach(function(elements){
        var ele;
        if(typeof elements.ele !== "undefined"){
            ele = elements.ele;
        }else{
            ele = elements;
        }
        ele.css('visibility','hidden');
    })
    $(window).on('scroll', function () {
        listElement.forEach(function(elements){
            var ele;
            var hasNext = false;
            if(typeof elements.ele !== "undefined"){
                ele = elements.ele;
                hasNext = elements.hasNext;
            }else{
                ele = elements;
            }
            var top_of_element = ele.offset().top;
            var bottom_of_element = ele.offset().top + ele.outerHeight();
    
            var top_of_screen = $(window).scrollTop();
            var bot_of_screen = top_of_screen + $(window).innerHeight();
            var view = top_of_screen + ($(window).innerHeight() / 1);
    
            if ((top_of_element < bot_of_screen) && (top_of_element > top_of_screen)) {
                
                if(hasNext  === true){
                    textillateCallStack(ele);
                }else{
                    ele.textillate({ 
                        in: {
                            effect: 'fadeInUp',
                        },
                    })
                }
            }
        })
    });

    function textillateCallStack(ele){
        if(ele.length <= 0 ){ return false }
        ele.textillate({ 
            in: {
                effect: 'fadeInUp',
            },
            callback: function () {
                textillateCallStack(ele.next("p"));
            },
        })
    }


    /**
     * ANIMATION
     */
    var arrAnimation = [
        $(".you"),
        $(".expertise .envysion__slide"),
        $(".establishment .envysion__slide"),
        $(".indepen.c-indepen .indepen__txt p"),
    ]
    var listElementAnimation= [];
    arrAnimation.forEach(function(ele){
        if(ele.length == 0){return false}
        if(ele.length > 1){
            ele.each(function(i , childEle){
                listElementAnimation.push($(childEle));
            });
        }else{
            listElementAnimation.push(ele);
        }
    });
    // listElementAnimation.forEach(function(ele){
    //     ele.css('visibility','hidden');
    // })
    $(window).on('scroll', function () {
        listElementAnimation.forEach(function(ele){
            var top_of_element = ele.offset().top + 50;
            var bottom_of_element = ele.offset().top + ele.outerHeight();
    
            var top_of_screen = $(window).scrollTop();
            var bot_of_screen = top_of_screen + $(window).innerHeight();
            var view = top_of_screen + ($(window).innerHeight() / 1);
    
            if ((top_of_element < bot_of_screen) && (top_of_element >= top_of_screen)) {
                ele.addClass("active-animation");
            }
        })
    });
    $(window).trigger("scroll");
}

var showModalDisclaimer = function(){
    if(!!  localStorage.getItem('policyAgree' ) === false){
        $("#disclaimer").modal("show");
        localStorage.setItem('policyAgree' , true); 
    }

    $(".policy__agree").on("click" , function(e){
        e.preventDefault();
        localStorage.setItem('policyAgree' , true); 
        $("#disclaimer").modal("hide");
    });
}

    $(window).on('load', function () {
        // typingText();
        // scrollBanner();
        expertiseMeaningSlider();
        expertiseMeaningTextSlider();
        // parallaxBanner();
        initEqualHeight($(".meaning__list-item > .meaning__list-ttl"));
        initEqualHeight($(".meaning__text-slide .imgText1__content-ttl.text-gradient"));
        clickScrollToElement();
        showModalDisclaimer();
    });

    $(window).on('resize' , function(){
        expertiseMeaningSlider();
    });

    var firstLog = sessionStorage.getItem('logged');

    if(!!firstLog === true){
        $("#loading").hide();
        initScrollToAddClass();
        animation();
        $("body").css("overflow","auto");
        $("body").scrollTop(0);
        $(".mainVisual__txt.typing").css('animation-delay','0s');
        $(".index .envysion__slide .mainVisual__btn").css('animation-delay','2s');
    }else{
        sessionStorage.setItem('logged', true);
        setTimeout(function(){
            $("#loading").animate({
                opacity:0
            },500, function(){
                $(this).hide();
            });
            initScrollToAddClass();
            animation();
            $("body").css("overflow","auto");
            $("body").scrollTop(0);
            $(".mainVisual__txt.typing").css('animation-delay','6.5s');
            $(".index  .envysion__slide .mainVisual__btn").css('animation-delay','8.5s');
        },6500);
    }
})